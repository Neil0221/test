document.getElementById("my-btn").addEventListener("click",getJson);

function getJson(){
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.onreadystatechange = function(){
        if(this.readyState === 4 && this.status === 200){
            var myObj = JSON.parse(this.responseText);
            showJson(myObj);
        }
    }
    xmlHttp.open("GET","json.php",true);
    xmlHttp.send();
}

function showJson(myObj){
    let txt = "";
    for(let i in myObj){
        txt += "<b>" + i + ": </b>"
                + myObj[i] + "<br>";
    }
    document.getElementById("demo").innerHTML = txt;
}